$dotnet_version_suffix = $env:APPVEYOR_REPO_BRANCH
if ($dotnet_version_suffix -match "^(feature|future)([-_]\#?\d+)?[-_]?") 
{ 
	$dotnet_version_suffix = ($dotnet_version_suffix -replace "^(feature|future|fix|bug)[-_]\#?\d*[-_]*") 
} 
$dotnet_version_suffix = $dotnet_version_suffix -replace "\.","-";
$dotnet_version_suffix = $dotnet_version_suffix -replace "_","-";
$dotnet_version_suffix = $dotnet_version_suffix -replace "#","";

if ($dotnet_version_suffix.Length -gt 20) 
{ 
	$dotnet_version_suffix = $dotnet_version_suffix.Substring(0, 20) 
}

$dotnet_version_suffix = $dotnet_version_suffix + '-' + $env:APPVEYOR_BUILD_NUMBER

if ($env:APPVEYOR_REPO_BRANCH -eq "master") 
{ 
	$dotnet_version_suffix = "" 
}

$project_path = 'src/' + $env:APPVEYOR_PROJECT_NAME
Write-Host "project_path: $project_path, version_suffix: $dotnet_version_suffix"

if ($dotnet_version_suffix)
{
    dotnet build $project_path -c $env:CONFIGURATION --no-dependencies --version-suffix $dotnet_version_suffix
    dotnet pack $project_path -c $env:CONFIGURATION --no-build --version-suffix $dotnet_version_suffix -o artifacts
}
else
{
    dotnet build $project_path -c $env:CONFIGURATION --no-dependencies
    dotnet pack $project_path -c $env:CONFIGURATION --no-build -o artifacts
}