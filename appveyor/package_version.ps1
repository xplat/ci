if ($env:APPVEYOR_REPO_BRANCH -eq "master") 
{ 
	$package_version = $env:APPVEYOR_BUILD_VERSION
}
else
{
    $package_version_suffix = $env:APPVEYOR_REPO_BRANCH
    if ($package_version_suffix -match "^(feature|future)([-_]\#?\d+)?[-_]?") 
    { 
        $package_version_suffix = ($package_version_suffix -replace "^(feature|future|fix|bug)[-_]\#?\d*[-_]*") 
    } 
    $package_version_suffix = $package_version_suffix -replace "\.","-";
    $package_version_suffix = $package_version_suffix -replace "_","-";
    $package_version_suffix = $package_version_suffix -replace "#","";

    if ($package_version_suffix.Length -gt 20) 
    { 
        $package_version_suffix = $package_version_suffix.Substring(0, 20) 
    }

    $package_version = $env:APPVEYOR_BUILD_VERSION -replace "\.$env:APPVEYOR_BUILD_NUMBER$", "-$package_version_suffix-$env:APPVEYOR_BUILD_NUMBER"
}

Write-Host "package_version: $package_version"

Set-AppveyorBuildVariable -Name 'package_version' -Value $package_version
