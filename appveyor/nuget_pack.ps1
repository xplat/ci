$nuget_version = $env:APPVEYOR_REPO_BRANCH
if ($nuget_version -match "^(feature|future)([-_]\d+)?[-_]?") 
{ 
	$nuget_version = ($nuget_version -replace "^(feature|future|fix|bug)[-_]\d*[-_]*") 
} 
$nuget_version = $nuget_version -replace "\.","-";
$nuget_version = $nuget_version -replace "_","-";

$nuget_version = "-" + $nuget_version;
if ($env:APPVEYOR_REPO_BRANCH -eq "master") 
{ 
	$nuget_version = "" 
}

$xml = [xml](Get-Content $env:NUSPEC_PATH)

foreach($node in $xml.package.metadata.dependencies.dependency)
{
	if ($node.Version -match "-.?")
	{
		$nuget_version = $nuget_version + "-beta"
		break
	}	
}  

if ($nuget_version.Length -gt 20) 
{ 
	$nuget_version = $nuget_version.Substring(0, 20) 
}

$version = $env:APPVEYOR_BUILD_VERSION + $nuget_version

nuget pack $env:NUSPEC_PATH -Version $version
